#/bin/bash

declare -a arr=(\
	"Next" \
	"Previous" \
	"Pause" \
	"PlayPause" \
	"Stop" \
	"Play" \
)

function printUsage {
	printf "USAGE:\n\t$0 METHOD\n\nAvailable methods:\n"
	printf "\t%s\n" "${arr[@]}"
	exit 0
}

if [ "$#" -ne 1 ]; then
    printUsage
fi

if [[ " ${arr[@]} " =~ " $1 " ]]; then
	PL=`dbus-send --session --dest=org.freedesktop.DBus \
	--type=method_call \
	--print-reply \
	/org/freedesktop/DBus org.freedesktop.DBus.ListNames \
	| grep org.mpris.MediaPlayer2 \
	| grep -Po "(?<=\").*?(?=\")"`

	plArray=(${PL// /})

	for i in "${plArray[@]}"
	do
		dbus-send --print-reply --dest=$i /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.$1
	done
else
	printUsage
fi