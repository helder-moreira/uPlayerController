# uPlayerController
#### Universal Player Controller
uPlayerController is a simple script that allows the control of audio or video independently of the player being used. The supported methods are:

  - Next
  - Previous
  - Pause
  - PlayPause
  - Stop
  - Play

I have developed this script when I moved from gnome to XFCE, and missed a way of controlling the music I was listening to using my keyboard's media keys, independently of the player I was using.

Using this script, you can map any key to a method which will be triggered on any player that is running.

As an example (in my case using XFCE4), I have mapped the key "XF86AudioPlay" to "UPC PlayPause" (moved upc.sh to /usr/local/bin and renamed to UPC).